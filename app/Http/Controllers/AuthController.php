<?php

namespace App\Http\Controllers;

use App\Models\User;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        if (empty($email) or empty($password)) {
            return response()->json(['status' => 'error', 'message' => 'fill missing fields']);
        }

        $client = new Client();

        try {
            return $client->post(config('service.passport.login_endpoint'), [
                "form_params" => [
                    "client_secret" => config('service.passport.client_secret'),
                    "grant_type" => "password",
                    "client_id" => config('service.passport.client_id'),
                    "username" => $request->email,
                    "password" => $request->password
                ]
            ]);
        } catch (BadResponseException $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function register(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $password = $request->password;
        $bin = $request->bin;
        $phone1 = $request->phone_one;
        $phone2 = $request->phone_two;
        $phone3 = $request->phone_three;
        $phone4 = $request->phone_four;
        $phone5 = $request->phone_five;
        $image = $request->image;

        // checks if required fields are filled
        if (empty($name) or empty($email) or empty($password) or empty($bin) or empty($phone1)) {
            return response()->json(['status' => 'error', 'message' => 'fill missing fields']);
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return response()->json(['status' => 'error', 'message' => 'incorrect email']);
        }

        if (strlen($password) < 8) {
            return response()->json(['status' => 'error', 'message' => 'too short password']);
        }

        // checks if user with this email exists
        if (User::where('email', '=', $email)->exists()) {
            return response()->json(['status' => 'error', 'message' => 'already signed up']);
        }

        try {
            $user = new User();
            $user->name = $name;
            $user->email = $email;
            $user->password = app('hash')->make($password);
            $user->bin = $bin;
            $user->phone_one = $phone1;

            // next if stmts are for optional phone nums
            if (!empty($phone2)) {
                $user->phone_two = $phone2;
            }
            if (!empty($phone3)) {
                $user->phone_three = $phone3;
            }
            if (!empty($phone4)) {
                $user->phone_four = $phone4;
            }
            if (!empty($phone5)) {
                $user->phone_five = $phone5;
            }

            // image upload
            if ($request->hasFile('image')) {
                $original_filename = $request->file('image')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                $destination_path = './upload/user/';
                $image = 'U-' . time() . '.' . $file_ext;

                if ($request->file('image')->move($destination_path, $image)) {
                    $user->image = '/upload/user/' . $image;

                } else {
                    return response()->json(['status' => 'error', 'message' => 'cannot upload file']);
                }
            } else {
                return response()->json(['status' => 'error', 'message' => 'file not found']);
            }

            if ($user->save()) {
                //return $this->login($request);
                return response()->json(['status' => 'success', 'message' => 'signed up successfully']);
            }

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function logout(Request $request)
    {
        try {
            auth()->user()->tokens()->each(function ($token) {
                $token->delete();
            });
            return response()->json(['status' => 'success', 'message' => 'signed out']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
        }
    }

}
