<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
    public function index(Request $request) {
        //return response(Post::all());
        return Post::all();
    }

    public function create(Request $request) {
        try {
            $post = new Post();
            $post->title = $request->title;
            $post->body = $request->body;

            if ($post->save()) {
                return response()->json(['status' => 'success', 'message' => 'success']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'success', 'message' => $e->getMessage()]);
        }
    }

    public function update(Request $request, $id) {
        try {
            $post = Post::findOrfail($id);
            $post->title = $request->title;
            $post->body = $request->body;

            if ($post->save()) {
                return response()->json(['status' => 'success', 'message' => 'success']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'success', 'message' => $e->getMessage()]);
        }
    }

    public function delete($id) {
        try {
            $post = Post::findOrfail($id);

            if ($post->delete()) {
                return response()->json(['status' => 'success', 'message' => 'deleted successfully']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'success', 'message' => $e->getMessage()]);
        }
    }
}
