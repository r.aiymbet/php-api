<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Excel;

class ImportExcelController extends Controller
{
    function index()
    {
        $data = DB::table('import_products')->orderBy('id', 'DESC')->get();
        return view('import_excel', compact('data'));
    }

    function import(Request $request)
    {
        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        if ($data->count() > 0) {
            foreach ($data->toArray() as $key => $value) {
                foreach ($value as $row) {
                    $insert_data[] = array(
                        'no_pp' => $row['Number p/p'],
                        'no_c3' => $row['Number C3'],
                        'ship' => $row['Ship'],
                        'departure_date' => $row['Departure Date'],
                        'carriage' => $row['Carriage'],
                        'waybill' => $row['Waybill'],
                        'expeditor' => $row['Expeditor'],
                        'shipping_name' => $row['Shipping Name'],
                        'gross_weight' => $row['Gross Weight'],
                        'status' => $row['Status']
                    );
                }
            }

            if (!empty($insert_data)) {
                DB::table('import_products')->insert($insert_data);
            }
        }
        return back()->with('success', 'Excel Data Imported successfully.');
    }
}

