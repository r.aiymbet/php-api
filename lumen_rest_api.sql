-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 28, 2021 at 07:52 AM
-- Server version: 5.7.32
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lumen_rest_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `import_products`
--

DROP TABLE IF EXISTS `import_products`;
CREATE TABLE `import_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_pp` int(11) DEFAULT NULL,
  `no_c3` int(11) DEFAULT NULL,
  `ship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `departure_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `carriage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `waybill` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expeditor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gross_weight` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2021_03_31_122649_create_posts_table', 1),
(2, '2021_04_01_102340_create_users_table', 2),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 3),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 3),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 3),
(6, '2016_06_01_000004_create_oauth_clients_table', 3),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 3),
(8, '2021_04_08_100408_add_fields_to_users', 4),
(9, '2021_04_08_114130_add_one_more_to_users', 5),
(10, '2021_04_12_115244_create_import_products_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('680066265bc766761028fddf20c9fa8c4e21b9fde4de51894b1dcc47d0f75b470661d79caa053e82', 12, 2, NULL, '[]', 0, '2021-04-08 11:30:51', '2021-04-08 11:30:51', '2022-04-08 11:30:51'),
('8549a81c22b17923a57aab1945bbdd1dbad4f125e5e603e81590f1855f82037929ea60f8a472304f', 11, 2, NULL, '[]', 0, '2021-04-08 10:40:54', '2021-04-08 10:40:54', '2022-04-08 10:40:54'),
('9263025286f1e18909243d7a55d7048e0c00f722ce0306703b703577c2e58828e5bddabf9002c6f7', 10, 2, NULL, '[]', 0, '2021-04-08 10:39:30', '2021-04-08 10:39:30', '2022-04-08 10:39:30'),
('e780de1018765e17b1d17a876cd565dc649aebb0fc5e2760b83e4a3c844ba9171b0d51ce0903ebf8', 8, 2, NULL, '[]', 0, '2021-04-08 09:24:14', '2021-04-08 09:24:14', '2022-04-08 09:24:14');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Lumen Personal Access Client', 'o4P7QU6Iu8ToNJvHOMXy61MwWbttVijDCTM2Ylgv', NULL, 'http://localhost', 1, 0, 0, '2021-04-01 10:59:37', '2021-04-01 10:59:37'),
(2, NULL, 'Lumen Password Grant Client', 'Q6tF1uzfZ9kcJahyM6VZSG2aBGQVCECM1oNULO3f', 'users', 'http://localhost', 0, 1, 0, '2021-04-01 10:59:37', '2021-04-01 10:59:37');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-04-01 10:59:37', '2021-04-01 10:59:37');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('0a366897c4c13265b25c6cb01b25a5f2084eb764f42b994046ac8f147eeb91b51c1b28caa1e6e995', 'a517237e3ae0c84179fd9e6daeb4bad938d45e9938dd129966887f00c0935498bb6e68c4c77b8d04', 0, '2022-04-01 12:17:27'),
('0c829c82a7d6d5828321b9b1a24f10b961759b193c40eb17dcaca9102ebc32c8ee3e293debfe9434', 'ae861da7769ba98a4134a9b22f57aa70bbc086e3bab0ea87965100d92a59532bf3652f90a4da6945', 0, '2022-04-01 12:07:55'),
('1f5c2d6d6ff3a84cda9e8c538f579326437fa6958d9af44253ccc2133a5738c6bd0e5e4675bd41b8', '9263025286f1e18909243d7a55d7048e0c00f722ce0306703b703577c2e58828e5bddabf9002c6f7', 0, '2022-04-08 10:39:30'),
('22e8c5d6743a3db01c8cc6e73f91517e04eba67ad72de59cef747c431fc4755e3fbaa79ef9b60bfa', 'e52c12b747ce0609d135ec3c1ede3283bf32b4c748bfcd6dbbb5839eaf3da397c4e06e1a549a61a5', 0, '2022-04-01 12:40:20'),
('233d3ed891890fd723807306c26e5b35225ccb9c894ae30ac8d6826a99b7b563f9e6ca8af939ed66', 'a5cd97bfc8b8bf75fd2a4c2b6be67b1ef656b3c95e41c80a86f7b53a16fc3ee66990d52c948f2ffa', 0, '2022-04-01 11:05:12'),
('25a38e6a105592e561b2226f6db69c87c8892e02a85ad362fb81a2482e079882421bb4589a188840', 'fef7f2943b52cd8c08d00ff19ad3c84f861344791a9bb94c30fe6007b2030e3589cf89179558479f', 0, '2022-04-01 13:17:59'),
('2ce2396532d26e1a94c24913d4e6fbf1d585d599944daa2e50c333dabcc3933ec2893b90c1c0ee22', '2ba02ca547a3a56891c1f3ed6648162a466de82d2f1c9db3036b375964bd3769083f0186e2f5a23f', 0, '2022-04-01 12:17:21'),
('3d251c5f1742c375ffef54e44a7eb5c7abc45ffe8d41868af43fbdab6de0eda4fe7eff358f38b12e', 'e780de1018765e17b1d17a876cd565dc649aebb0fc5e2760b83e4a3c844ba9171b0d51ce0903ebf8', 0, '2022-04-08 09:24:14'),
('4682a4a9e0da6d729e6a2fdd8c66a71badca8f1c4fa6cb92abcb994b3c7bd4f88614d7f7d12c8409', '67502367ac09613cd2a590c7d5898fc7365f9e3dfd082f888fe3bd8391794c75edb84c4cce1a80f3', 0, '2022-04-08 09:22:13'),
('5259d429c6599d7a31f4d446126209942bb29650c59b1d9a65b2e024e613a7010cbf9c5ab68e0ea5', '76d47c713b3f319ceae4ec549cee6358d398f13d3e501c9e03ae5c3774305a1a9c27e6ced8d0f94b', 0, '2022-04-01 11:47:13'),
('87288470b17ae3deb908cfd5088bfff4a988e20a0185da388a3f74efec3988be7a8f4f10d4df72db', '97959d1899cc85a4a5c171f4e979e9436a5e1849f95eeac30c76ecd883843d4e4e873bfceed1b387', 0, '2022-04-01 12:53:13'),
('a95fd78e63b5445e48eec45612519c73c8614b1ba972496c27c75a6e4c9d3477342615b19d76e033', '930840ca6da932ca683bedf0afab07fb95611597fe009f804f7c3ce3f106d9ee45c662f26b019435', 0, '2022-04-01 13:48:35'),
('ba4b096f5762661c8f69242eb053e1ca6c788414993682c21eaaebf6f8bd2137753b3b8a8c7ab1c0', '983fc59894eabe77a753fa63e4201ecf6f1c065f012b9bf1c3960a58e66326fecafd8a0d457f4269', 0, '2022-04-08 10:35:13'),
('c507df815104e511a7c53446f6d44a6c1e443577762f5d06b2042fc2121d63760261940a6e564c3d', '85a24f94d5f235e4d1a29d58ee3f251dab7c0c401122750e417112cd1de24ef444ecaddd8de53b9c', 0, '2022-04-08 10:37:00'),
('d484fa84af58302d4c93a8a8804eb1771bd8cababa30e89c50675723f2f9e8219308235e22083c32', '680066265bc766761028fddf20c9fa8c4e21b9fde4de51894b1dcc47d0f75b470661d79caa053e82', 0, '2022-04-08 11:30:51'),
('dd886d0f692efffb5ed925795f71874beb3028891a9196a62aaf3c43b089cce515a56248b7fa1683', '6a44ba4a40d4ad578efda5a76afcfe8b32c7da9e5db9626693198c009d0ae106fef7daf9470e0b2c', 0, '2022-04-01 12:02:37'),
('f7d27f030cbc8923ae84660d418791500d3cc75f0f484cf43dca017516ad1f881c987451a31dfff1', '8549a81c22b17923a57aab1945bbdd1dbad4f125e5e603e81590f1855f82037929ea60f8a472304f', 0, '2022-04-08 10:40:54');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, 'Non enim et aspernatur odit id possimus tenetur.', 'Quia mollitia qui saepe nisi ullam. Alias et velit numquam ipsa sequi ratione. Repudiandae totam est ut commodi hic eligendi aliquam. A voluptate quibusdam ut qui reiciendis cupiditate ea perferendis. Eligendi totam maxime debitis.', '2021-03-31 12:45:32', '2021-03-31 12:45:32'),
(2, 'Est nihil aut porro sit.', 'Ut fuga quisquam qui autem. Expedita quia laborum et eveniet ad.', '2021-03-31 12:45:32', '2021-03-31 12:45:32'),
(3, 'Aspernatur dolorem quibusdam quidem aut modi eius similique.', 'Rem sint in cupiditate explicabo non rerum. Culpa a voluptate ea distinctio et totam. Commodi eos voluptatem consequuntur asperiores aut.', '2021-03-31 12:45:32', '2021-03-31 12:45:32'),
(4, 'Debitis sint dolor ipsa perferendis.', 'Magni facere harum est placeat reprehenderit. Hic delectus explicabo voluptatem et quam officia. Voluptas ipsum eius officiis possimus autem.', '2021-03-31 12:45:32', '2021-03-31 12:45:32'),
(5, 'Laboriosam eos voluptates ad dicta ex a.', 'Vel omnis officiis aperiam quis iusto quae. Beatae rerum commodi itaque dicta. Labore corrupti voluptas sed. Consectetur dolorem officiis molestiae fuga maiores esse doloremque.', '2021-03-31 12:45:32', '2021-03-31 12:45:32'),
(6, 'Eaque explicabo sint asperiores quisquam omnis est.', 'Non aut qui ut officia. Non cupiditate temporibus omnis voluptas ad a sint iusto. Labore numquam minus maxime et quia sit et. A sunt dicta magni ad cum dolore eum ducimus.', '2021-03-31 12:45:32', '2021-03-31 12:45:32'),
(7, 'Aut temporibus facere esse tempore qui sint et.', 'A velit mollitia delectus laborum. Non quasi ullam tenetur quod culpa vel asperiores laudantium. Rerum distinctio nostrum repellat quibusdam asperiores debitis totam ipsum. Nihil ex et incidunt temporibus.', '2021-03-31 12:45:32', '2021-03-31 12:45:32'),
(8, 'Exercitationem quam at autem vel.', 'Debitis totam libero in. Esse atque ad ratione officiis culpa itaque eos. Aut nobis consequatur expedita pariatur fuga dolorem quo. Labore in nesciunt ut ratione quam impedit. Dolorum hic sunt eum ipsam quaerat pariatur.', '2021-03-31 12:45:32', '2021-03-31 12:45:32'),
(9, 'Sit commodi aut aut ut at dolorem consequatur.', 'Et provident quasi sunt rerum commodi deleniti voluptatem. Reprehenderit dolor impedit sint impedit voluptas corporis qui. Ad consequuntur necessitatibus assumenda itaque sunt.', '2021-03-31 12:45:32', '2021-03-31 12:45:32'),
(10, 'Consequatur est nam natus id sequi porro sapiente magni.', 'Molestiae exercitationem esse deserunt et. Pariatur ut sit dolor voluptatum. Voluptas est necessitatibus voluptatem sed.', '2021-03-31 12:45:32', '2021-03-31 12:45:32'),
(12, 'Title 2', 'New body text', '2021-04-01 10:02:54', '2021-04-01 10:02:54'),
(13, 'New Title 15', 'New body text 15', '2021-04-08 09:23:27', '2021-04-08 09:23:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `bin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_one` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_three` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_four` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_five` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`, `bin`, `phone_one`, `phone_two`, `phone_three`, `phone_four`, `phone_five`, `image`) VALUES
(1, 'Eugene Bailey', 'kertzmann.carleton@example.org', '$2y$10$kJk6ZWwx9KUyge.dQLsum.oDstA1xJMj/arQd26GwMZkkfnyrgZFy', '2021-04-01 10:30:22', '2021-04-01 10:30:22', '', '', NULL, NULL, NULL, NULL, NULL),
(2, 'Miss Lilla Kunze DDS', 'eromaguera@example.org', '$2y$10$U/ojzvSIIGmhTu7SjAqpfuR9C6tsH2PixHVtzRD1CGdH0XUK23tjG', '2021-04-01 10:30:22', '2021-04-01 10:30:22', '', '', NULL, NULL, NULL, NULL, NULL),
(3, 'Rashawn Spinka', 'marge27@example.com', '$2y$10$U0v2xlA8D00RlXJjBmp3Q.IPLYO97hYVUKLL6jjHq54La5kr.d786', '2021-04-01 10:30:22', '2021-04-01 10:30:22', '', '', NULL, NULL, NULL, NULL, NULL),
(4, 'Prof. Gia Kreiger', 'josefina.gibson@example.com', '$2y$10$aQConn9pgC6xxDA9J3KLvuOAHtjhXP1YSPBF1lAOfgJxSv5VB2X/C', '2021-04-01 10:30:22', '2021-04-01 10:30:22', '', '', NULL, NULL, NULL, NULL, NULL),
(5, 'Domenica Hickle', 'concepcion.okon@example.net', '$2y$10$ncfab9F3rv44hrdg3pMrGeGFPwJ3YdCtBQDo6x6akHbnUqLd3G3BS', '2021-04-01 10:30:22', '2021-04-01 10:30:22', '', '', NULL, NULL, NULL, NULL, NULL),
(6, 'John Doe', 'jdoe@example.com', '$2y$10$HsjvpTXJfsLcstB9oN1G7.mnC/vkK2bN5bbWfR/2ZnlEWIkLdwaoC', '2021-04-01 12:36:44', '2021-04-01 12:36:44', '', '', NULL, NULL, NULL, NULL, NULL),
(7, 'Jane Doe', 'janedoe@example.com', '$2y$10$UG8J8gWfnBDppq0tYf01KuN25Z3hP0H4lQlUHBaIWgGZ9XW90l0ve', '2021-04-01 12:40:20', '2021-04-01 12:40:20', '', '', NULL, NULL, NULL, NULL, NULL),
(8, 'John Smith', 'jsmith@example.com', '$2y$10$ikfISTPXXdnAzE43cZQg7.arb5zkbKqeAs6ytFB/zIzRwSej1ZFva', '2021-04-08 09:24:14', '2021-04-08 09:24:14', '', '', NULL, NULL, NULL, NULL, NULL),
(9, 'Johnny Barron', 'jbr@example.com', '$2y$10$kXZqUb5Q4I2LEAjhEKPBouNFATpd7OI3lF70Jkjsm/wMuaj2c5jQG', '2021-04-08 10:35:12', '2021-04-08 10:35:12', '01234', '+123456789', NULL, NULL, NULL, NULL, NULL),
(10, 'Helen Smith', 'hsm@example.com', '$2y$10$PSsyVl1YfHsw6tRei2BDsOQs4g5qkUC8UO/Xu8rn76PE/MBsVWlNW', '2021-04-08 10:39:29', '2021-04-08 10:39:29', '01256', '+123456789', '+123456789', NULL, NULL, NULL, NULL),
(11, 'Helen Johnson', 'hj@example.com', '$2y$10$0pOOt78WLqXBeCVzAJ0ynuK5n.1PIjcFEiobjha4yO2DV100hLZKe', '2021-04-08 10:40:54', '2021-04-08 10:40:54', '012567', '+123456789', '+123456789', '+123456789', NULL, NULL, NULL),
(12, 'Lisa Muller', 'limuller@example.com', '$2y$10$E.1KeqzRJihAfAhqKj3Ss.BobnilXhm.hUgeG4XFDZW48KhxrKe4m', '2021-04-08 11:30:50', '2021-04-08 11:30:50', '012563', '+123456789', '+123456789', '+123456789', NULL, NULL, '/upload/user/U-1617881450.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `import_products`
--
ALTER TABLE `import_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `import_products`
--
ALTER TABLE `import_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
