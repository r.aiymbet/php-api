<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_products', function (Blueprint $table) {
            $table->id();
            $table->integer('no_pp')->nullable();
            $table->integer('no_c3')->nullable();
            $table->string('ship')->nullable();
            $table->string('departure_date')->nullable();
            $table->string('carriage')->nullable();
            $table->string('waybill')->nullable();
            $table->string('expeditor')->nullable();
            $table->string('shipping_name')->nullable();
            $table->string('gross_weight')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
